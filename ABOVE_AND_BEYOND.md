COMP229 A2  
Nicholas Weston (44882017)

## Above-and-beyond discussion

Unfortunately I didn't have all that much time to complete this assignment to my own standards, and the use of the
decorator pattern has turned me off from doing much more with this assignment. (If the purpose of this assignment is to
traumatise us from ever using the decorator pattern, it has worked for me!)

Nevertheless I managed to do the following:
- Redesigned the implementation from scratch, since I was discontent with the `bos` library and the solutions for the
workshop tasks.
- Created my own graphics for the characters using Inkscape.
- A `Stage` is now loaded from a plain text file listing "commands" on how to construct the `Stage`. The files are
explained further in the JavaDoc for `StageReader`. I came across the idea of text files formatted like this while
looking through the game files of *Hearts of Iron IV* and *Europa Universalis IV* (both by Paradox Interactive).
- Spent a good six hours learning about the wonders of logging and implemented loggers and log files
into the assignment.
- Wrote JavaDocs for almost everything. (I'm a little obsessive.)