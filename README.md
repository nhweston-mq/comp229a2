COMP229 A2  
Nicholas Weston (44882017)

## Adherence to the decorator pattern

This solution adheres to the decorator pattern in the implementation of `Character` and its subclasses. The decorator
pattern itself is applied to change the movement and appearance of `Character` upon entering a `Cell` with a certain
`Terrain`.

### Class hierarchy and structure

The class hierarchy of `Character` and its subclasses follows that of the decorator pattern.

Below is a class diagram illustrating the relationships between `Character` and its subclasses. 

![alt text](class_diagram.png "Class diagram of Character and its subclasses.")

Key:
- A rectangle without rounded edges represents a non-abstract class.
- A rectangle with rounded edges represents an abstract class.
- An open pointed arrow from *X* to *Y* means that "X is a subclass of Y".
- A diamond "arrow" from *X* to *Y* means that "X is an instance variable of Y".

The `Character` class is equivalent to the *abstract component*, the `Sheep`, `Shepherd`, `Player`, and `Wolf` are the
*concrete components*, `ChDecorator` is the *abstract decorator*, and `ChDecSlow` is a *concrete decorator*. In some
implementations of the decorator pattern, the abstract component is an interface, however, it is also within the scope
of the pattern to use an abstract class.

### Delegation of responsibilities through a decorator stack

A key feature of the decorator pattern is the *encapsulation* of objects within other objects that are
indistinguishable from each other in their interfacing with external classes. This can be applied iteratively to form
what can be visualised as a *stack* or a series of layers that each contain another layer beneath. To achieve this, the
decorating class delegates all method calls to the decorated object (i.e. the next node or layer). If the decorated
object is itself an instance of a decorating class, it too will delegate the method call to its decorated object. This
continues until the core concrete component is reached.

This can be seen in the `ChDecorator` class, where almost all methods in `Character` (with the exception of a few
relating to manipulation of the decorator stack itself) are overriden and simply delegate the method to the decorated
object. Eventually the calls will reach a concrete component, i.e. `Sheep`, `Wolf`, etc. at which point the calls
terminate since the method definition will be taken from `Character` (`Sheep`, `Wolf`, etc.).

The concrete decorator, `ChDecSlow`, is responsible for adding functionality. For most methods, the role of the
decorator is impertinent. In other words, only the core `Character` is relevant to the output or effect of the method.
In such cases, the calls through the decorator stack will eventually reach the core `Character` as previously
described, without changing anything. `ChDecorator` has already defined this behaviour. The purpose of the `ChDecSlow`
class, and any concrete decorator in a decorator pattern, is to define functionality that should be *added*. The only
delegating methods that should be overwritten in a concrete decorator are the ones where it is intended for the
decorator to impact behaviour. In this case, these methods are `paint(Graphics)` and `nextTurn(int)`. Respectively,
these are to draw a dot on the `Character` graphic, and to deduct movement upon preparation for the character's next
turn. These methods overwrite from `ChDecorator` but call upon `super`. This is so that they make their contribution to
the method output or effect, and then continue calling through the decorator stack.

### Addition of functionality through constructors

In the decorator pattern, the functionality of an entity is added to by invoking, somewhat counter-intuitively, a
constructor. In effect, the decorator created by the construction becomes the *head* or *parent* of the decorator
stack. The constructor is always passed the current head of the decorator stack as a parameter, which it then stores as
an instance variables. This is what forms the link between successive decorators in the decorator stack. This can be
seen in the `switch` block on Line 203 of `Character`.

### Dynamic changes in functionality

The decorator pattern allows the functionality of an object to be modified at runtime, and more flexibly and
dynamically than is permitted by subclassing. Subclassing is linear in that an object cannot inherit from multiple
classes, unless the multiple classes themselves form a linear hierarchy of inheritance. The decorator pattern addresses
this in a way that permits an object to inherit behaviour not only arbitrary combination, but in arbitrary cardinality.
In this implementation for example, a `Character` may have any number of `ChDecSlow` affecting it.

## Solving the un-decorating problem

The specifications for the distinction part are challenging because they necessitate that it be possible to *remove*
decorators, not just add them. (This notion will henceforth be referred to as *un-decorating*.) In particular, the
following issues must be addressed:
- There must be a capacity to distinguish between decorators with different properties (in this example, the `Terrain`
associated with `ChDecSlow`).
- Any part of the decorator stack must be removeable.
- Following on from the previous point, the head of the decorator stack is a potentially problematic case since all
references to the head will require updating.
- The remainder of the decorator stack must remain intact. (This is of course a fairly trivial point, but is easier
said than done.)

The greatest guiding intuition in my solution is that the decorator pattern is very much analagous to a linked list, in
that both involve entities that add data or behaviour, but also *point* to another entity. My solution removes
decorators in a similar fashion to how an item in a linked list is removed.

An important feature in my decorator implementation is that each `Character` stores a `head`, representing the
outermost decorator in the decorator stack. This allows for references in other classes to not require persistent
updating whenever a new decorator is added. When a new decorator is added, it uses the delegation mechanism to
nominally set itself as the new head of all decorators in its stack. As will be discussed further, this is crucial to
initialising the un-decorating process itself, and in addressing the special case where the head of the decorator stack
is removed.

In summary:
1. A `Character` seeking to un-decorate itself will call `unDec(ChDecorator, ChDecorator)` on the head of the decorator
stack. The first parameter represents the previous element in the stack, which for the initial case, is `null`. The
second parameter is an instance of a subclass of `ChDecorator` that resembles, i.e. is functionally equivalent to, a
decorator that should be removed.
2. Each decorator in the stack that is traversed by the method calls will compare itself to the second parameter. If
the decorator in question is not equivalent to the parameter, it calls the method on its decorated `Character` with
`this` as the first parameter and the same second parameter.
3. If a decorator *is* equivalent to the second parameter, the successive method calls terminate and the decorated
`Character` of the previous decorator (i.e. the first parameter) is set to the decorated `Character` of *this*
`Character`. This is just like how a linked list will remove an element by changing the link of the previous element to
point to the next element. The decorator is thence removed.

If no such decorator exists, the method calls will continue to the core concrete `Character` and terminate without
changing anything.

Step 3 in the summary above is different in the special case where the decorator to be removed is the head of the
stack. In this case, the decorator is simply removed by setting the head of the decorated `Character` to the decorated
`Character` itself. Through delegation, all `Character` instances in the decorator stack will be updated to reflect
that what was the *second* `Character` in the stack is now the head.

I believe my solution to be optimal given the constraints of having to use the decorator pattern. However, the
decorator pattern is a very poor design decision in the specifications. In particular:
- It is wasteful in that all decorators have numerous unused fields inherited from `Character`.
- It necessitates that all methods be overwritten in the abstract decorator class for the purpose of delegation. This
code is tedious to write and semantically redundant.
- There are numerous method calls involved in what would usually be a single method call, and most of these calls are
merely delegating. This is inefficient, and is prone to a `StackOverflowException`.
- The functionality with these particular specifications need not be implemented by the decorator pattern nor
subclassing `Character`. A class representing "ailments" or "modifiers" to characters could be created, with instances
associated with a `Character` (in a many-to-one relationship respectively, using a `Collection` in `Character`).