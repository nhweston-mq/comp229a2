/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.logging.*;

/**
 * The main class for the <i>Sheep and Wolves</i> applet.
 */
public class Main extends JFrame implements Runnable {

    private static final Logger l = Logger.getLogger("io");

    /**
     * The width of the window.
     */
    static final int WIN_DIM_X = 1280;

    /**
     * The height of the window.
     */
    static final int WIN_DIM_Y = 720;

    /**
     * The background colour.
     */
    static final Color COL_BG = Color.BLACK;

    /**
     * The stroke colour of each {@link Cell}.
     */
    static final Color COL_STROKE = Color.WHITE;

    /**
     * The colour of the {@link Cell} highlight. The transparency of the highlight is accounted for by the alpha of
     * this colour.
     */
    static final Color COL_HIGHLIGHT = new Color(255, 255, 255, 128);

    /**
     * The names of the logs maintained by the applet.
     */
    private static final String[] LOG_NAMES = {"ai", "game", "io"};

    /**
     * The file path of the {@link Stage} to load upon initialising the applet. Note that the <code>Stage</code> is
     * reset, the default <code>Stage</code> specified by {@link Stage} will be loaded, not this.
     */
    private static final String STAGE_FILE_PATH = "stages/stage1.txt";

    /**
     * The number of milliseconds per frame. Note that changing this will affect the timing of all game mechanics, not
     * just the repainting of graphics.
     */
    private static final int MILLIS_PER_FRAME = 20;

    /**
     * The main method for the <i>Sheep and Wolves</i> applet.
     *
     * @param args  unused.
     */
    public static void main(String[] args) {
        try {
            initLogging();
        }
        catch (IOException e) {
            System.out.println("Failed to initialise logging!");
        }
        Main main = new Main();
        main.run();
    }

    /**
     * Initialises logging.
     *
     * @throws IOException  if an error occurs reading the configuration file or setting up log files.
     */
    private static void initLogging() throws IOException {
        FileInputStream configFile = new FileInputStream("logs/config.properties");
        LogManager.getLogManager().readConfiguration(configFile);
        for (String logName : LOG_NAMES) {
            Logger logger = Logger.getLogger(logName);
            logger.addHandler(new ConsoleHandler());
            logger.addHandler(new FileHandler("logs/" + logName + ".log"));
        }
    }

    /**
     * Represents the content of the window's interior. Responsible for handling input and output.
     */
    private class Canvas extends JPanel implements KeyListener, MouseMotionListener {

        /**
         * Creates a new <code>Canvas</code> with the dimensions specified by {@link #WIN_DIM_X} and
         * {@link #WIN_DIM_Y}.
         */
        Canvas() {
            l.info("Initialising Canvas.");
            setPreferredSize(new Dimension(WIN_DIM_X, WIN_DIM_Y));
        }

        /**
         * Updates the {@link Stage}, then repaints it.
         *
         * @param g     the <code>Graphics</code> context to paint to.
         */
        @Override
        public void paint(Graphics g) {
            stage.update();
            stage.paint(g);
        }

        /**
         * Called when a key is <i>typed</i>. If <code>'r'</code> is typed, the {@link Stage} is reloaded. Otherwise,
         * the key is delegated to {@link Stage}.
         *
         * @param e     the <i>KeyEvent</i> corresponding to this method call.
         */
        @Override
        public void keyTyped(KeyEvent e) {
            char key = e.getKeyChar();
            l.info("The \'" + key + "\' key has been typed.");
            if (key == 'r') {
                l.info("Reloading Stage...");
                stage = new Stage();
            }
            stage.inputKey(e.getKeyChar());
        }

        /**
         * Does nothing.
         *
         * @param e     the <i>KeyEvent</i> corresponding to this method call.
         */
        @Override
        public void keyPressed(KeyEvent e) {}

        /**
         * Does nothing.
         *
         * @param e     the <i>KeyEvent</i> corresponding to this method call.
         */
        @Override
        public void keyReleased(KeyEvent e) {}

        /**
         * Called when the mouse is moved. If the mouse is within the window (i.e. {@link #getMousePosition()} does not
         * return <code>null</code>), its position is delegated to {@link Stage}.
         *
         * @param e     the <i>KeyEvent</i> corresponding to this method call.
         */
        @Override
        public void mouseMoved(MouseEvent e) {
            Point mousePos = getMousePosition();
            if (mousePos != null) {
                stage.inputMouse(mousePos.x, mousePos.y);
            }
        }

        /**
         * Does nothing.
         *
         * @param e     the <i>MouseEvent</i> corresponding to this method call.
         */
        @Override
        public void mouseDragged(MouseEvent e) {}

    }

    private Stage stage;
    private Canvas canvas;

    /**
     * Initialises the applet. Instantiates an instance of <code>Main</code> and then calls {@link #run()}.
     */
    private Main() {
        l.info("Initialising Main...");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        canvas = new Canvas();
        stage = new Stage(STAGE_FILE_PATH);
        setContentPane(canvas);
        addKeyListener(canvas);
        addMouseMotionListener(canvas);
        pack();
        setVisible(true);
        l.info("Initialised Main successfully.");
    }

    /**
     * Runs the applet.
     */
    public void run() {
        l.info("Running applet.");
        while (canvas.isVisible()) {
            Instant timeInit = Instant.now();
            repaint();
            Instant timeFinal = Instant.now();
            try {
                Thread.sleep(MILLIS_PER_FRAME - Duration.between(timeInit, timeFinal).toMillis());
            }
            catch (InterruptedException e) {
                l.warning("Main thread was interrupted.");
                e.printStackTrace();
            }
        }
        l.info("Applet terminated.");
    }

}
