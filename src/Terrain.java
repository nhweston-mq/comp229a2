/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Enum representing the different terrain types of a {@link Cell}.
 */
enum Terrain {

    ROCK    (new Color(192, 192, 192), new Color(255, 255, 255, 192)),
    DIRT    (new Color(128,  64,   0), new Color(192,  96,   0, 192)),
    GRASS   (new Color(128, 255,   0), new Color(160, 255,  64, 192)),
    TREES   (new Color(  0, 128,  64), new Color(  0, 192,  96, 192));

    private static final Logger l = Logger.getLogger("terraingen");

    private Color col;
    private Color colVariant;

    /**
     * Creates a new <code>Terrain</code> enum, assigning the specified <code>int</code> as its associated identifier,
     * and the specified {@link Color} to render it as.
     *
     * @param col           the <code>Color</code> to render a {@link Cell} with this <code>Terrain</code> as.
     * @param colVariant    a variant <code>Color</code> to use for graphics related to the <code>Terrain</code> other
     *                      than a <code>Cell</code>.
     */
    Terrain(Color col, Color colVariant) {
        this.col = col;
        this.colVariant = colVariant;
    }

    /**
     * Returns the {@link Color} to render this as.
     *
     * @return  the <code>Color</code> to render a {@link Cell} with this <code>Terrain</code> as.
     */
    public Color getCol() {
        return col;
    }

    /**
     * Returns the variant {@link Color} of this <code>Terrain</code>.
     *
     * @return  the variant <code>Color</code> of this <code>Terrain</code>.
     */
    public Color getColVariant() {
        return colVariant;
    }

}