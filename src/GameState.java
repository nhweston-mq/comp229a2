/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

/**
 * Represents the state of a game (i.e. {@link Stage}).
 */
enum GameState {

    LOADING,
    USER_TURN,
    TURN_TRANSITION,
    GAME_OVER_WIN,
    GAME_OVER_LOSS

}
