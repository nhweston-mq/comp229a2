/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.util.logging.Logger;

/**
 * Represents a <code>Player</code>. A <code>Player</code> is controlled by the user and can move one {@link Cell} per
 * turn.
 */
class Player extends Character {

    private static final Logger l = Logger.getLogger("game");

    /**
     * {@inheritDoc}
     */
    Player(Stage stage, Cell cell) {
        super(stage, cell);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default <code>Behaviour</code> of a <code>Player</code> is {@link BhvrUserControlled}.</p>
     */
    @Override
    void setBehaviour() {
        setBehaviour(new BhvrUserControlled(this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void nextTurn(int numMoves) {
        super.nextTurn(numMoves + 1);
    }

}
