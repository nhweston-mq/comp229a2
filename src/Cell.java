/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Represents a <code>Cell</code> in a {@link Grid}.
 */
class Cell {

    private static final Logger l = Logger.getLogger("game");

    /**
     * The default {@link Terrain}.
     */
    private static final Terrain DEF_TERRAIN = Terrain.GRASS;

    private Grid grid;
    private int id;
    private int posX, posY;
    private int size;
    private Terrain terrain;
    private Set<Character> chs;

    /**
     * Creates a new <code>Cell</code> within the specified {@link Grid}, with the specified ID, x-position, y-position
     * and side length.
     *
     * @param grid  the <code>Grid</code> this <code>Cell</code> is a part of.
     * @param id    the ID of the <code>Cell</code>, determined from left-to-right, then top-to-bottom.
     * @param posX  the x-position in the window.
     * @param posY  the y-position in the window.
     * @param size  the side length of the <code>Cell</code>.
     */
    Cell(Grid grid, int id, int posX, int posY, int size) {
        this.grid = grid;
        this.id = id;
        this.posX = posX;
        this.posY = posY;
        this.size = size;
        setTerrain(DEF_TERRAIN);
    }

    /**
     * Paints the <code>Cell</code>.
     *
     * @param g     the <code>Graphics</code> context to paint to.
     */
    void paint(Graphics g) {
        g.setColor(getTerrain().getCol());
        g.fillRect(posX, posY, size, size);
        g.setColor(Main.COL_STROKE);
        g.drawRect(posX, posY, size, size);
    }

    /**
     * Paints a highlight over the <code>Cell</code>.
     *
     * @param g     the <code>Graphics</code> context to paint to.
     */
    void paintHighlight(Graphics g) {
        g.setColor(Main.COL_HIGHLIGHT);
        g.fillRect(posX, posY, size, size);
    }

    /**
     * Informs the <code>Cell</code> that the specified {@link Character} has entered it. Returns either
     * <code>GAME_OVER_WIN</code>, <code>GAME_OVER_LOSS</code> or <code>null</code>, representing the implications on
     * the game of this move.
     *
     * @param ch    the <code>Character</code> that is entering.
     * @return      the <code>GameState</code> resulting from this move.
     * @see #checkChConditions()
     */
    GameState chEnter(Character ch) {
        l.fine(ch.toString() + " has entered " + toString() + ".");
        if (chs == null) {
            chs = new HashSet<>();
        }
        chs.add(ch);
        if (chs.size() > 1) {
            l.info(toString() + " contains multiple Characters.");
            for (Character contained : chs) {
                l.info(contained.toString() + " is in " + toString() + ".");
            }
            return checkChConditions();
        }
        return null;
    }

    /**
     * Informs the <code>Cell</code> that the specified {@link Character} has exited it.
     *
     * @param ch    the <code>Character</code> that is exiting.
     */
    void chExit(Character ch) {
        l.info(ch.toString() + " has left " + toString() + ".");
        chs.remove(ch);
        if (chs.isEmpty()) {
            chs = null;
        }
    }

    /**
     * Assigns the specified {@link Terrain} to the <code>Cell</code>.
     *
     * @param terrain   the desired <code>Terrain</code>.
     */
    void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    /**
     * Returns the <code>Cell</code> adjacent to this <code>Cell</code> in the specified {@link Direction}. If no such
     * <code>Cell</code> exists, returns <code>null</code>.
     *
     * @param dir   the desired <code>Direction</code>.
     * @return      the <code>Cell</code> adjacent in the specified <code>Direction</code>.
     */
    Cell getAdjacentCell(Direction dir) {
        int x = id%grid.getDimX();
        int y = id/grid.getDimX();
        switch (dir) {
            case UP:    y--;    break;
            case DOWN:  y++;    break;
            case LEFT:  x--;    break;
            case RIGHT: x++;    break;
        }
        return grid.getCellByXY(x, y);
    }

    /**
     * Returns the x-coordinate of the <code>Cell</code> in the {@link Grid}.
     *
     * @return  the x-coordinate of the <code>Cell</code>.
     */
    int getX() {
        return id%grid.getDimX();
    }

    /**
     * Returns the y-coordinate of the <code>Cell</code> in the {@link Grid}.
     *
     * @return  the y-coordinate of the <code>Cell</code>.
     */
    int getY() {
        return id/grid.getDimX();
    }

    /**
     * Returns the x-position of the top-left corner of <code>Cell</code> in the window.
     *
     * @return  the x-position of the <code>Cell</code>.
     */
    int getPosX() {
        return posX;
    }

    /**
     * Returns the y-position of the top-left corner of <code>Cell</code> in the window.
     *
     * @return  the y-position of the <code>Cell</code>.
     */
    int getPosY() {
        return posY;
    }

    /**
     * Returns the side length of the <code>Cell</code>.
     *
     * @return  the side length of the <code>Cell</code>.
     */
    int getSize() {
        return size;
    }

    /**
     * Returns the {@link Terrain} of this <code>Cell</code>.
     *
     * @return  the <code>Terrain</code> of this <code>Cell</code>.
     */
    Terrain getTerrain() {
        return this.terrain;
    }

    /**
     * <p>Returns a {@link String} representation of this <code>Cell</code>, along with its ID and coordinates, for
     * example:</p>
     *
     * <blockquote><code>Cell 42 (2, 3)</code></blockquote>
     *
     * @return  a <code>String</code> representation of this <code>Cell</code>.
     */
    public String toString() {
        return "Cell " + id + " (" + getX() + ", " + getY() + ")";
    }

    /**
     * Checks whether the {@link Character} instances in this <code>Cell</code> warrant the game to end. Invoked only
     * by {@link #chEnter(Character)} if there is more than one <code>Character</code> in this <code>Cell</code>.
     * Returns <code>GAME_OVER_WIN</code> if this <code>Cell</code> is cohabited by a {@link Sheep} and a
     * {@link Shepherd}, <code>GAME_OVER_LOSS</code> if this <code>Cell</code> is cohabited by a <code>Sheep</code> and
     * a {@link Wolf}, <code>null</code> otherwise. If this <code>Cell</code> contains a <code>Sheep</code>,
     * <code>Wolf</code>, and a <code>Shepherd</code>, the game is evaluated as a win.
     *
     * @return  the <code>GameState</code> representing the implication of the <code>Character</code> instances
     *          cohabiting this <code>Cell</code>.
     */
    private GameState checkChConditions() {
        boolean containsSheep = false;
        boolean containsShepherd = false;
        boolean containsWolf = false;
        for (Character ch : chs) {
            if (ch.getClass().equals(Sheep.class)) {
                l.fine(toString() + " contains a Sheep.");
                containsSheep = true;
                if (containsShepherd) {
                    l.info(toString() + " contains a Sheep and a Shepherd.");
                    return GameState.GAME_OVER_WIN;
                }
            }
            else if (ch.getClass().equals(Shepherd.class)) {
                l.fine(toString() + " contains a Shepherd.");
                containsShepherd = true;
                if (containsSheep) {
                    l.info(toString() + " contains a Sheep and a Shepherd.");
                    return GameState.GAME_OVER_WIN;
                }
            }
            else if (ch.getClass().equals(Wolf.class)) {
                l.fine(toString() + " contains a Wolf.");
                containsWolf = true;
            }
        }
        if (containsWolf && containsSheep) {
            l.info(toString() + " contains a Sheep and a Wolf.");
            return GameState.GAME_OVER_LOSS;
        }
        return null;
    }

}
