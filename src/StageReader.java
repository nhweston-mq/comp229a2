/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * <p>Reads stage files, allowing {@link Stage} to iterate through a list of initialisation commands. The stage files
 * are plain text files, containing the following structural components (or <i>tokens</i>):</p>
 *
 * <ul>
 *     <li>keys (as strings),</li>
 *     <li>textual values,</li>
 *     <li>the <code>'='</code> character, assigning a key to a value,</li>
 *     <li>the <code>'{'</code> and <code>'}'</code> characters, enclosing a tabular value.</li>
 * </ul>
 *
 * <p>All tokens must be delimited by whitespace.</p>
 *
 * <p>In this implementation, the outermost keys are commands, each containing a table of keys and textual values
 * defining various parameters related to that command. The commands are as follows:</p>
 *
 * <blockquote><table>
 *     <caption>The specifications for the stage files.</caption>
 *     <tbody>
 *         <tr>
 *             <th style="width: 15%">Command</th>
 *             <th style="width: 8px"></th>
 *             <th style="width: 35%">Description</th>
 *             <th style="width: 8px"></th>
 *             <th style="width: 50%">Parameters</th>
 *         </tr>
 *         <tr>
 *             <td><code>grid</code></td>
 *             <td></td>
 *             <td>Defines the dimensions of the {@link Grid} associated with the <code>Stage</code>.</td>
 *             <td></td>
 *             <td>
 *                 <ul>
 *                     <li><code>dim_x</code> - the <i>x</i>-dimension of the <code>Grid</code>.</li>
 *                     <li><code>dim_y</code> - the <i>y</i>-dimension of the <code>Grid</code>.</li>
 *                 </ul>
 *             </td>
 *         </tr>
 *         <tr>
 *             <td><code>create_character</code></td>
 *             <td></td>
 *             <td>Creates a {@link Character}. Note that if <code>random_cell</code> is set to <code>true</code>, the
 *             parameters <code>x</code> and <code>y</code> will be ignored, and do not have to be included.
 *             Conversely, the absence of <code>random_cell</code> is interpreted as <code>random_cell</code> having
 *             been set to <code>false</code>.</td>
 *             <td></td>
 *             <td>
 *                 <ul>
 *                     <li><code>type</code> - the type of <code>Character</code> in lower case (e.g.
 *                     <code>shepherd</code>).</li>
 *                     <li><code>random_cell</code> - <code>true</code> if the location of the <code>Character</code>
 *                     is to be randomly selected.</li>
 *                     <li><code>x</code> - the <i>x</i>-coordinate of the initial location of the
 *                     <code>Character</code>.</li>
 *                     <li><code>y</code> - the <i>y</i>-coordinate of the initial location of the
 *                     <code>Character</code>.</li>
 *                 </ul>
 *             </td>
 *         </tr>
 *         <tr>
 *             <td><code>set_terrain</code></td>
 *             <td></td>
 *             <td>Sets a rectangular area to a specified {@link Terrain} or utilises {@link TerrainGen} to set the
 *             <code>Terrain</code> for the entire <code>Grid</code>. Note that if <code>use_terrain_gen</code> is set
 *             to <code>true</code>, all other parameters ignored, and do not have to be included. Conversely, the
 *             absence of <code>use_terrain_gen</code> is interpreted as <code>use_terrain_gen</code> having been set
 *             to <code>false</code>.</td>
 *             <td></td>
 *             <td>
 *                 <ul>
 *                     <li><code>use_terrain_gen</code> - <code>true</code> if <code>TerrainGen</code> should be used
 *                     to set the <code>Terrain</code> of the entire <code>Grid</code> associated with this
 *                     <code>Stage</code>.</li>
 *                     <li><code>type</code> - the <code>Terrain</code> to set the rectangular area to.</li>
 *                     <li><code>x1</code> - the <i>x</i>-coordinate of the top-left corner of the rectangular
 *                     area.</li>
 *                     <li><code>y1</code> - the <i>y</i>-coordinate of the top-left corner of the rectangular
 *                     area.</li>
 *                     <li><code>x2</code> - the <i>x</i>-coordinate of the bottom-right corner of the rectangular
 *                     area.</li>
 *                     <li><code>x2</code> - the <i>y</i>-coordinate of the bottom-right corner of the rectangular
 *                     area.</li>
 *                 </ul>
 *             </td>
 *         </tr>
 *     </tbody>
 * </table></blockquote>
 *
 * <p>The use of these files are inspired from a similar implementation in <i>Hearts of Iron IV</i> and <i>Europa
 * Universalis IV</i> by Paradox Interactive.</p>
 */
class StageReader {

    private static Logger l = Logger.getLogger("io");

    private StageReaderCommand cursor;
    private LinkedList<StageReaderCommand> commands;

    /**
     * Represents a command (outer table) from the stage file.
     */
    private class StageReaderCommand {

        String command;
        Map<String, String> data;

        /**
         * Creates a new <code>StageReaderCommand</code> of the specified type.
         *
         * @param command   the type of the command.
         */
        StageReaderCommand(String command) {
            this.command = command;
            data = new Hashtable<>();
        }

        /**
         * Adds the specified key-value pair (both textual) to the command.
         *
         * @param key       the key to add.
         * @param value     the value to associate to the key.
         */
        void put(String key, String value) {
            data.put(key, value);
        }

        /**
         * Returns the type of command.
         *
         * @return  the type of command.
         */
        String getCommand() {
            return command;
        }

        /**
         * Returns the value associated with the specified key.
         *
         * @param key   the key of the desired value.
         * @return      the value associated with the specified key.
         */
        String getData(String key) {
            return data.get(key);
        }

    }

    /**
     * Creates a new <code>StageReader</code>, storing stage configuration data stored in the specified stage file.
     *
     * @param filePath      the path of stage file to read from.
     * @throws IOException  if an error occurs in reading the file.
     */
    StageReader(String filePath) throws IOException {
        FileReader fr;
        BufferedReader br;
        Scanner sc;
        commands = new LinkedList<>();
        fr = new FileReader(filePath);
        br = new BufferedReader(fr);
        sc = new Scanner(br);
        while (sc.hasNext()) {
            commands.add(new StageReaderCommand(sc.next()));
            if (!sc.next().equals("=")) {
                throw new SyntaxException("Expected \"=\" near \"" + commands.getLast() + "\".");
            }
            if (!sc.next().equals("{")) {
                throw new SyntaxException("Expected \"{\" near \"" + commands.getLast() + "\".");
            }
            String token = sc.next();
            while (!token.equals("}")) {
                if (!sc.next().equals("=")) {
                    throw new SyntaxException("Expected \"=\" near \"" + token + "\".");
                }
                commands.getLast().put(token, sc.next());
                token = sc.next();
            }
        }
        sc.close();
        br.close();
        fr.close();
    }

    /**
     * Returns the type of the next {@link StageReaderCommand} and moves the cursor to it. The previous command is
     * discarded.
     *
     * @return  the next <code>StageReaderCommand</code>.
     */
    String nextCommand() {
        if (commands.isEmpty()) {
            return null;
        }
        cursor = commands.removeFirst();
        return cursor.getCommand();
    }

    /**
     * Returns the value associated with the specified key in the {@link StageReaderCommand} at the cursor.
     *
     * @param key   the key of the desired value.
     * @return      the value associated with the specified key.
     */
    String getData(String key) {
        return cursor.getData(key);
    }

    /**
     * Returns an <code>int</code> representing the value associated with the specified key in the
     * {@link StageReaderCommand} at the cursor. If the value cannot be parsed, <code>-1</code> is returned.
     *
     * @param key   the key of the desired value.
     * @return      the <code>int</code> value associated with the specified key.
     * @see Integer#parseInt(String)
     */
    int getDataAsInt(String key) {
        String data = cursor.getData(key);
        try {
            return Integer.parseInt(data);
        }
        catch (NullPointerException e) {
            l.warning("Key \"" + key + "\" has no value associated to it. Returning -1.");
        }
        catch (NumberFormatException e) {
            l.warning("Key \"" + key + "\" is associated with \"" + data + "\", integer expected. Returning -1.");
        }
        return -1;
    }

    /**
     * Returns a <code>boolean</code> representing the value associated with the specified key in the
     * {@link StageReaderCommand} at the cursor.
     *
     * @param key   the key of the desired value.
     * @return      the <code>boolean</code> value associated with the specified key.
     */
    boolean getDataAsBoolean(String key) {
        String data = cursor.getData(key);
        return Boolean.parseBoolean(data);
    }

}
