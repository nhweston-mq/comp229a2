/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.logging.Logger;

/**
 * Represents a {@link Character} decorator.
 */
abstract class ChDecorator extends Character {

    private static final Logger l = Logger.getLogger("game");

    private Character chDec;

    /**
     * Creates a new <code>ChDecorator</code>.
     */
    ChDecorator() {}

    /**
     * Creates a new <code>ChDecorator</code> decorating the specified {@link Character}.
     *
     * @param chDec     the <code>Character</code> to decorate.
     */
    ChDecorator(Character chDec) {
        this.chDec = chDec;
        setHead(this);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void paint(Graphics g) {
        chDec.paint(g);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void nextTurn(int numMoves) {
        chDec.nextTurn(numMoves);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void nextFrame(int frame) {
        chDec.nextFrame(frame);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void nextMove() {
        chDec.nextMove();
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> checks whether this instance is equivalent to the target. If so, this instance is
     * removed from the decorator stack. If not, it delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void unDec(ChDecorator prev, ChDecorator target) {
        if (!isDecEquiv(target)) {
            chDec.unDec(this, target);
            return;
        }
        l.info("Undecorating from " + toString() + ".");
        if (prev == null) {
            chDec.setHead(chDec);
            return;
        }
        if (chDec.isDecEquiv(target)) {
            prev.setChDec(chDec);
        }
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void setCell(Cell cell) {
        chDec.setCell(cell);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void setBehaviour() {
        chDec.setBehaviour();
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    void setBehaviour(Behaviour behaviour) {
        chDec.setBehaviour(behaviour);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> sets its own head accordingly, then delegates this method to the decorated
     * {@link Character}.</p>
     */
    @Override
    void setHead(Character head) {
        super.setHead(head);
        chDec.setHead(head);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    Class getChType() {
        return chDec.getChType();
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    Stage getStage() {
        return chDec.getStage();
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    Cell getCell() {
        return chDec.getCell();
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    String toStringRoot() {
        return chDec.toStringRoot();
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecorator</code> delegates this method to the decorated {@link Character}.</p>
     */
    @Override
    public String toString() {
        String root = chDec.toStringRoot();
        return (getClass().getSimpleName() + " (" + hashCode() + ") for " + root);
    }

    /**
     * Sets the {@link Character} decorated by this <code>ChDecorator</code>.
     *
     * @param chDec     the <code>Character</code> to decorate.
     */
    private void setChDec(Character chDec) {
        this.chDec = chDec;
    }

}
