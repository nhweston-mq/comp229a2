/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.Random;

/**
 * A {@link Character} decorator reducing the movement of a <code>Character</code>. Acquired when a
 * <code>Character</code> moves into a {@link Cell} with <code>GRASS</code> or <code>DIRT</code> {@link Terrain}.
 * When a <code>Character</code> traverses <code>TREES</code>, a <code>ChDecSlow</code> with <code>GRASS</code> is
 * removed from its decorator stack (if one exists). This likewise applies to traversing <code>ROCK</code> for a
 * <code>ChDecSlow</code> with <code>DIRT</code>. A <code>ChDecSlow</code> is graphically represented as a small dot on
 * the <code>Character</code>.
 */
class ChDecSlow extends ChDecorator {

    /**
     * The radius of the circles drawn on the {@link Character}.
     */
    private static final int RADIUS = 3;

    /**
     * The <code>Color</code> of the stroke of the dots.
     */
    private static final Color COL_STROKE = new Color(0, 0, 0, 192);

    /**
     * Used to randomly generate the relative position of the circle.
     */
    private static Random random = new Random();

    private Terrain terrain;
    private int posX, posY;

    /**
     * Creates a new <code>ChDecSlow</code> with the specified {@link Terrain} to decorate the specified
     * {@link Character}.
     *
     * @param chDec     the <code>Character</code> to decorate.
     * @param terrain   the <code>Terrain</code> associated with this <code>ChDecSlow</code>.
     */
    ChDecSlow(Character chDec, Terrain terrain) {
        super(chDec);
        this.terrain = terrain;
        setRandomPos();
    }

    /**
     * Creates a new <code>ChDecSlow</code> with the specified {@link Terrain}.
     *
     * @param terrain   the <code>Terrain</code> associated with this <code>ChDecSlow</code>.
     */
    ChDecSlow(Terrain terrain) {
        this.terrain = terrain;
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecSlow</code> paints a small dot on the {@link Character} using the variant {@link Color} of the
     * {@link Terrain}.</p>
     */
    @Override
    void paint(Graphics g) {
        super.paint(g);
        g.setColor(terrain.getColVariant());
        g.fillOval(getCell().getPosX() + posX, getCell().getPosY() + posY, RADIUS*2, RADIUS*2);
        g.setColor(COL_STROKE);
        g.drawOval(getCell().getPosX() + posX, getCell().getPosY() + posY, RADIUS*2, RADIUS*2);
    }

    /**
     * {@inheritDoc}
     *
     * <p><code>ChDecSlow</code> decreases the number of moves by one.</p>
     */
    @Override
    void nextTurn(int numMoves) {
        super.nextTurn(numMoves - 1);
    }

    /**
     * {@inheritDoc}
     *
     * <p>In the case of <code>ChDecSlow</code>, this method returns <code>true</code> if the other {@link Character}
     * is an instance of <code>ChDecSlow</code>, and has the same associated {@link Terrain} as this.</p>
     */
    @Override
    boolean isDecEquiv(Character otherCh) {
        if (otherCh instanceof ChDecSlow) {
            ChDecSlow other = (ChDecSlow) otherCh;
            if (this.terrain == other.terrain) {
                return true;
            }
        }
        return false;
    }

    /**
     * Randomly sets the relative position of the dot.
     */
    private void setRandomPos() {
        posX = random.nextInt(getCell().getSize()) - RADIUS;
        posY = random.nextInt(getCell().getSize()) - RADIUS;
    }

}
