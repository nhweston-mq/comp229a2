/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Represents a grid, composed of {@link Cell} instances.
 */
class Grid {

    private static final Logger l = Logger.getLogger("game");

    /**
     * The margin along the x-axis between the <code>Grid</code> and the edge of the window.
     */
    private static final int MARGIN_X = 10;

    /**
     * The margin along the y-axis between the <code>Grid</code> and the edge of the window.
     */
    private static final int MARGIN_Y = 10;

    /**
     * Used to obtain a random {@link Cell} in the <code>Grid</code>.
     */
    private static Random random = new Random();

    private int posX, posY;
    private int dimX, dimY;
    private int cellSize;
    private Cell[] cells;
    private Cell highlight;

    /**
     * Creates a new <code>Grid</code> with the specified dimensions.
     *
     * @param dimX  the desired number of columns.
     * @param dimY  the desired number of rows.
     */
    Grid(int dimX, int dimY) {
        setPos(MARGIN_X, MARGIN_Y);
        setDim(dimX, dimY);
        initCells();
    }

    /**
     * Paints the <code>Grid</code>.
     *
     * @param g     the <code>Graphics</code> context to paint to.
     */
    void paint(Graphics g) {
        Logger.getLogger("io").finer("Painting Grid...");
        g.setColor(Main.COL_BG);
        g.fillRect(0, 0, Main.WIN_DIM_X, Main.WIN_DIM_Y);
        for (Cell cell : cells) {
            cell.paint(g);
        }
        if (highlight != null) {
            highlight.paintHighlight(g);
        }
        Logger.getLogger("io").finer("Completed painting Grid.");
    }

    /**
     * Generates and sets the {@link Terrain} of each {@link Cell} using a new instance of {@link TerrainGen}.
     */
    void genTerrain() {
        TerrainGen terrainGen = new TerrainGen(dimX, dimY);
        for (int i=0; i<cells.length; i++) {
            cells[i].setTerrain(terrainGen.get(i));
        }
    }

    /**
     * Sets all {@link Cell} instances in a rectangular region of the <code>Grid</code> to the specified
     * {@link Terrain}.
     *
     * @param x1        the x-coordinate of the top-left corner of the region.
     * @param y1        the y-coordinate of the top-left corner of the region.
     * @param x2        the x-coordinate of the bottom-right corner of the region.
     * @param y2        the y-coordinate of the bottom-right corner of teh region.
     * @param terrain   the desired <code>Terrain</code>.
     */
    void setTerrain(int x1, int y1, int x2, int y2, Terrain terrain) {
        for (int x=x1; x<=x2; x++) for (int y=y1; y<=y2; y++) {
            getCellByXY(x, y).setTerrain(terrain);
        }
    }

    /**
     * Sets the {@link Cell} at the specified position in the window as the highlighted <code>Cell</code>.
     *
     * @param posX  the x-position of the desired <code>Cell</code>.
     * @param posY  the y-position of the desired <code>Cell</code>.
     */
    void setHighlight(int posX, int posY) {
        Cell prevHighlight = highlight;
        highlight = getCellByPos(posX, posY);
        if (prevHighlight != highlight) {
            if (highlight != null) {
                l.fine("Set " + highlight.toString() + " as highlighted Cell.");
            }
            else {
                l.fine("Set highlighted Cell to null.");
            }
        }
    }

    /**
     * Returns a random {@link Cell} in the <code>Grid</code>.
     *
     * @return  a random <code>Cell</code>.
     */
    Cell getRandomCell() {
        return cells[random.nextInt(cells.length)];
    }

    /**
     * Returns the {@link Cell} in the given column and row.
     *
     * @param x     the column of the desired <code>Cell</code>.
     * @param y     the row of the desired <code>Cell</code>.
     * @return      the <code>Cell</code> at the given position.
     */
    Cell getCellByXY(int x, int y) {
        if (x < 0 || x >= dimX || y < 0 || y >= dimY) {
            l.finer("There is no Cell at Grid coordinates (" + x + ", " + y + ").");
            return null;
        }
        Cell result = getCellById(y*dimX + x);
        if (result != null) {
            l.finer("Cell at Grid coordinates (" + x + ", " + y + ") is Cell " + result.toString() + ".");
            return result;
        }
        l.warning("(" + x + ", " + y + ") are valid Grid coordinates but no Cell is associated with it.");
        return null;
    }

    /**
     * Returns the width of the <code>Grid</code>.
     *
     * @return  the width of the <code>Grid</code>.
     */
    int getDimX() {
        return dimX;
    }

    /**
     * Instantiates the {@link Cell} instances.
     */
    private void initCells() {
        l.info("Initialising Cells...");
        optimiseCellSize();
        cells = new Cell[dimX*dimY];
        for (int i=0; i<cells.length; i++) {
            cells[i] = new Cell(this, i, posX + (i%dimX)*cellSize, posY + (i/dimX)*cellSize, cellSize);
        }
        l.info("Cells initialised successfully.");
    }

    /**
     * Sets the <code>Cell</code> size to be as large as possible without extending beyond the window margins.
     */
    private void optimiseCellSize() {
        l.info("Optimising Cell size...");
        int maxCellSizeX = (Main.WIN_DIM_X - 2* MARGIN_X) / dimX;
        int maxCellSizeY = (Main.WIN_DIM_Y - 2* MARGIN_Y) / dimY;
        int result = (Math.min(maxCellSizeX, maxCellSizeY));
        l.info("Optimal Cell size is " + result + ".");
        setCellSize(result);
    }

    /**
     * Sets the size of the {@link Cell} instances in pixels. This method should not be invoked following
     * {@link #initCells()}.
     *
     * @param cellSize  the desired <code>Cell</code> size.
     */
    private void setCellSize(int cellSize) {
        this.cellSize = cellSize;
    }

    /**
     * Sets the position of the top-left corner of this <code>Grid</code> in the window. This method should not be
     * invoked following {@link #initCells()}.
     *
     * @param posX  the desired x-position of the <code>Grid</code>.
     * @param posY  the desired y-position of the <code>Grid</code>.
     */
    private void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
        l.info("Grid position set to (" + posX + ", " + posY + ").");
    }

    /**
     * Sets the width and height of the <code>Grid</code>. This method should not be invoked following
     * {@link #initCells()}.
     *
     * @param dimX  the desired width.
     * @param dimY  the desired height.
     */
    private void setDim(int dimX, int dimY) {
        this.dimX = Math.max(dimX, 2);
        this.dimY = Math.max(dimY, 2);
        l.info("Grid dimensions set to " + dimX + "x" + dimY + ".");
    }

    /**
     * Returns the {@link Cell} at the specified position in the window.
     *
     * @param posX  the x-position of the desired <code>Cell</code>.
     * @param posY  the y-position of the desired <code>Cell</code>.
     * @return      the <code>Cell</code> at the specified position.
     */
    private Cell getCellByPos(int posX, int posY) {
        int x = (posX - this.posX) / cellSize;
        int y = (posY - this.posY) / cellSize;
        Cell result = getCellByXY(x, y);
        if (result != null) {
            l.finer("Cell at position (" + posX + ", " + posY + ") is " + result.toString() + ".");
            return result;
        }
        l.finer("There is no Cell at position (" + posX + ", " + posY + ").");
        return null;
    }

    /**
     * Returns the {@link Cell} with the specified ID.
     *
     * @param id    the ID of the desired <code>Cell</code>.
     * @return      the <code>Cell</code> with the specified ID.
     */
    private Cell getCellById(int id) {
        if (id >= 0 && id < cells.length) {
            return cells[id];
        }
        l.finer("Cell " + id + " does not exist.");
        return null;
    }

}