/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

/**
 * Thrown by {@link StageReader} if a stage file contains a syntax error.
 */
class SyntaxException extends RuntimeException {

    /**
     * Creates a new <code>SyntaxException</code> with the specified message.
     *
     * @param message   the desired message for this exception.
     * @see StageReader#StageReader(String)
     */
    SyntaxException(String message) {
        super(message);
    }

}
