/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

/**
 * Represents a <code>Sheep</code>. A <code>Sheep</code> has three movement. The game ends when a <code>Sheep</code> is
 * in the same {@link Cell} as a {@link Shepherd} or {@link Wolf}.
 */
class Sheep extends Character {

    /**
     * {@inheritDoc}
     */
    Sheep(Stage stage, Cell cell) {
        super(stage, cell);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default <code>Behaviour</code> for a <code>Sheep</code> is {@link BhvrMoveTowards}, with the target an
     * instance of {@link Shepherd}.</p>
     */
    @Override
    void setBehaviour() {
        setBehaviour(new BhvrMoveTowards(this, Shepherd.class));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void nextTurn(int numMoves) {
        super.nextTurn(numMoves + 3);
    }

}
