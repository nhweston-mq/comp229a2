/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

/**
 * Represents a <code>Shepherd</code>. A <code>Shepherd</code> nominally has one movement, but stands still. The game
 * is won if a <code>Shepherd</code> is in the same {@link Cell} as a {@link Sheep}.
 */
class Shepherd extends Character {

    /**
     * {@inheritDoc}
     */
    Shepherd(Stage stage, Cell cell) {
        super(stage, cell);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default <code>Behaviour</code> for a <code>Shepherd</code> is {@link BhvrFreeze}.</p>
     */
    @Override
    void setBehaviour() {
        setBehaviour(new BhvrFreeze(this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void nextTurn(int numMoves) {
        super.nextTurn(numMoves + 1);
    }

}
