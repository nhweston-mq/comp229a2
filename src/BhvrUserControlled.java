/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.util.logging.Logger;

/**
 * Represents a <code>Behaviour</code> whereby the {@link Character} is controlled by the user.
 */
class BhvrUserControlled extends Behaviour {

    private static final Logger l = Logger.getLogger("ai");

    private Stage stage;

    /**
     * Creates a new <code>BhvrUserControlled</code> for the specified {@link Character}.
     *
     * @param ch    the <code>Character</code> to which this <code>Behaviour</code> is to be associated.
     */
    BhvrUserControlled(Character ch) {
        super(ch);
        l.info("Instantiating User Controlled Behaviour for " + ch.toString() + ".");
        this.stage = ch.getStage();
    }

    /**
     * <p>Returns a {@link Direction} corresponding to the move the associated {@link Character} should make.</p>
     *
     * <p>For <code>BhvrUserControlled</code>, this method will return {@link Stage#getUserMove()}.</p>
     *
     * @return  the <code>Direction</code> the <code>Character</code> should move in.
     */
    @Override
    Direction getNextMove() {
        l.info(getCh().toString() + " is user-controlled.");
        return stage.getUserMove();
    }

}
