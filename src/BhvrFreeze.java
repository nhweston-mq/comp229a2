/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.util.logging.Logger;

/**
 * Represents a <code>Behaviour</code> whereby the {@link Character} does not move at all. This is distinct from a
 * <code>Character</code> having no movement in that a move list is created, but all moves are <code>NONE</code>.
 */
class BhvrFreeze extends Behaviour {

    private static final Logger l = Logger.getLogger("ai");

    /**
     * Creates a new <code>BhvrFreeze</code> for the specified {@link Character}.
     *
     * @param ch    the <code>Character</code> to which this <code>Behaviour</code> is to be associated.
     */
    BhvrFreeze(Character ch) {
        super(ch);
        l.info("Instantiating Freeze Behaviour for " + ch.toString() + ".");
    }

    /**
     * <p>Returns a {@link Direction} corresponding to the move the associated {@link Character} should make.</p>
     *
     * <p>For <code>BhvrFreeze</code>, this method will always return <code>NONE</code>.</p>
     *
     * @return  the <code>Direction</code> the <code>Character</code> should move in.
     */
    @Override
    Direction getNextMove() {
        l.info(getCh().toString() + " is standing still.");
        return Direction.NONE;
    }

}
