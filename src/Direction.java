/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

/**
 * Represents a direction.
 */
enum Direction {

    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE

}
