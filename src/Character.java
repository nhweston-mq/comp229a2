/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Represents a character in the game. The subclasses of <code>Character</code> form a decorator pattern.
 */
abstract class Character {

    private static final Logger l = Logger.getLogger("game");
    private static final Logger lIO = Logger.getLogger("io");

    /**
     * The directory containing the character graphics.
     */
    private static final String gfxDir = "gfx/";

    /**
     * An array containing pairs of <code>Character</code> types (i.e. {@link Class} instances) and file paths to the
     * respective graphic.
     *
     * @see #gfx
     */
    private static final String[][] gfxFilePaths = {
            {"Player",      "player.png"},
            {"Sheep",       "sheep.png"},
            {"Shepherd",    "shepherd.png"},
            {"Wolf",        "wolf.png"}
    };

    /**
     * Associates each <code>Character</code> type (i.e. {@link Class}) to a {@link BufferedImage} representing the
     * <code>Character</code> type.
     */
    private static Map<Class, BufferedImage> gfx = new Hashtable<>();

    static {
        lIO.info("Initialising graphics...");
        for (String[] gfxFilePath : gfxFilePaths) {
            Class chType;
            try {
                chType = Class.forName("sheepandwolves." + gfxFilePath[0]);
                initGfx(chType, gfxDir + gfxFilePath[1]);
            }
            catch (IOException e) {
                lIO.severe("Failed to load graphics for \"" + gfxFilePath[0] +"\".");
            }
            catch (ClassNotFoundException e) {
                lIO.severe("The class \"" + gfxFilePath[0] + "\" does not exist.");
            }
        }
        lIO.info("Successfully initialised graphics.");
    }

    /**
     * Attempts to associate a <code>Character</code> type (i.e. {@link Class}) to a {@link BufferedImage}.
     *
     * @param chType        the type of <code>Character</code>.
     * @param filePath      the path of the graphics file.
     * @throws IOException  if an error occurs in reading the graphics file.
     */
    private static void initGfx(Class chType, String filePath) throws IOException {
        lIO.info("Reading graphics file \"" + filePath + "\" for " + chType.getSimpleName() + ".");
        gfx.put(chType, ImageIO.read(new File(filePath)));
    }

    private Stage stage;
    private Cell cell;
    private Behaviour behaviour;
    private LinkedList<Integer> framesToMove;
    private Character head;

    /**
     * Creates a new <code>Character</code> in the specified {@link Stage} and {@link Cell}.
     *
     * @param stage     the desired <code>Stage</code>.
     * @param cell      the desired <code>Cell</code>.
     */
    Character(Stage stage, Cell cell) {
        setStage(stage);
        setHead(this);
        setCell(cell);
    }

    /**
     * Creates a new <code>Character</code>.
     */
    Character() {}

    /**
     * Paints the <code>Character</code>.
     *
     * @param g     the <code>Graphics</code> context to paint to.
     */
    void paint(Graphics g) {
        Image image = gfx.get(getClass());
        int posX = cell.getPosX() + (cell.getSize() - image.getWidth(null)) / 2;
        int posY = cell.getPosY() + (cell.getSize() - image.getHeight(null)) / 2;
        g.drawImage(gfx.get(getClass()), posX, posY, null);
    }

    /**
     * Prepares this <code>Character</code> for the next turn. Creates a list of frames of when this
     * <code>Character</code> should move. The time between each move is evenly distributed across the length of the
     * turn transition.
     *
     * @param numMoves  the number of moves the <code>Character</code> has for the next turn.
     */
    void nextTurn(int numMoves) {
        if (behaviour == null) {
            setBehaviour();
        }
        l.info(toString() + " has " + numMoves + " moves.");
        if (numMoves <= 0) {
            return;
        }
        l.info("Preparing turn for " + toString() + ".");
        framesToMove = new LinkedList<>();
        int incr = Stage.NUM_FRAMES_PER_TURN / numMoves;
        for (int i=0; i<numMoves; i++) {
            framesToMove.add(i*incr);
        }
    }

    /**
     * Processes the given frame for this <code>Character</code>, calling {@link #nextMove()} if it is due to move.
     *
     * @param frame     the current frame of the turn transition.
     */
    void nextFrame(int frame) {
        l.finer("Processing move for " + toString() + ".");
        if (!framesToMove.isEmpty()) {
            if (frame > framesToMove.peek()) {
                nextMove();
                framesToMove.poll();
            }
        }
    }

    /**
     * Processes the next move.
     */
    void nextMove() {
        setCell(cell.getAdjacentCell(behaviour.getNextMove()));
    }

    /**
     * Un-decorates a {@link ChDecorator} equivalent to the parameter from this <code>Character</code>.
     *
     * @param prev      the previous <code>ChDecorator</code> in the decorator stack, or <code>null</code> if it is the
     *                  head.
     * @param target    a <code>ChDecorator</code> to search for in the decorator stack and remove.
     * @see #isDecEquiv(Character)
     */
    void unDec(ChDecorator prev, ChDecorator target) {}

    /**
     * Returns <code>true</code> if this and the parameter are equivalent decorators, <code>false</code> otherwise.
     *
     * @param other     the {@link Character} to compare this to.
     * @return          <code>true</code> if this and the parameter are equivalent decorators, <code>false</code>
     *                  otherwise.
     */
    boolean isDecEquiv(Character other) {
        return false;
    }

    /**
     * Sets the {@link Cell} on which this <code>Character</code> is located. The <code>Character</code> will be
     * decorated or un-decorated appropriately, unless the current <code>Cell</code> is <code>null</code> (i.e. the
     * <code>Character</code> has just been placed into the {@link Stage}). Will also trigger the game to end if
     * appropriate cohabitation conditions are met.
     *
     * @param cell  the <code>Cell</code> to move this <code>Character</code> to.
     */
    void setCell(Cell cell) {
        if (cell == null) {
            l.warning(toString() + " is attempting to move outside the board.");
            return;
        }
        Cell prevCell = this.cell;
        this.cell = cell;
        boolean initial = (prevCell == null);
        if (prevCell == this.cell) {
            l.fine(toString() + " is already in " + cell.toString() + ".");
            return;
        }
        GameState gameState = this.cell.chEnter(this);
        if (!initial) {
            prevCell.chExit(this);
            l.fine("Moving " + toString() + " from " + this.cell.toString() + " to " + cell.toString() + ".");
            switch (this.cell.getTerrain()) {
                case ROCK: {
                    l.fine("Attempting to undecorate " + toStringRoot() + " from ChDecSlow with DIRT.");
                    getHead().unDec(null, new ChDecSlow(Terrain.DIRT));
                    break;
                }
                case DIRT: {
                    l.fine("Decorating " + toStringRoot() + " with ChDecSlow with DIRT.");
                    new ChDecSlow(getHead(), Terrain.DIRT);
                    break;
                }
                case GRASS: {
                    l.fine("Decorating " + toStringRoot() + " with ChDecSlow with GRASS.");
                    new ChDecSlow(getHead(), Terrain.GRASS);
                    break;
                }
                case TREES: {
                    l.fine("Attempting to undecorate " + toStringRoot() + " from ChDecSlow with GRASS.");
                    getHead().unDec(null, new ChDecSlow(Terrain.GRASS));
                }
            }
        }
        if (gameState != null) {
            stage.endGame(gameState);
        }
    }

    /**
     * Sets the {@link Behaviour} of the <code>Character</code> to its default.
     */
    abstract void setBehaviour();

    /**
     * Assigns the specified {@link Behaviour} to the <code>Character</code>.
     *
     * @param behaviour     the desired <code>Behaviour</code>.
     */
    void setBehaviour(Behaviour behaviour) {
        l.info("Setting Behaviour of " + toString() + " to " + behaviour.toString() + ".");
        this.behaviour = behaviour;
    }

    /**
     * Sets the head of the decorator stack to the specified <code>Character</code>.
     *
     * @param head  the desired head of the decorator stack.
     */
    void setHead(Character head) {
        this.head = head;
    }

    /**
     * Returns the type of this <code>Character</code>, i.e. the {@link Class} of the root of the decorator stack.
     *
     * @return  the type of <code>Character</code>.
     */
    Class getChType() {
        return getClass();
    }

    /**
     * Returns the {@link Stage} this <code>Character</code> is a part of.
     *
     * @return  the <code>Stage</code> this <code>Character</code> is a part of.
     */
    Stage getStage() {
        return stage;
    }

    /**
     * Returns the {@link Cell} containing this <code>Character</code>.
     *
     * @return  the <code>Cell</code> containing this <code>Character</code>.
     */
    Cell getCell() {
        return cell;
    }

    /**
     * Returns the head of the decorator stack.
     *
     * @return  the head of the decorator stack.
     */
    Character getHead() {
        return head;
    }

    /**
     * Utility method to obtain the {@link String} representation of root <code>Character</code> in the decorator
     * stack.
     *
     * @return  the <code>String</code> representation of the root <code>Character</code> in the decorator stack.
     */
    String toStringRoot() {
        return toString();
    }

    /**
     * Returns a {@link String} representation of this <code>Character</code>.
     *
     * @return  a <code>String</code> representation of this <code>Character</code>.
     */
    @Override
    public String toString() {
        return (this.getClass().getSimpleName() + " (" + hashCode() + ")");
    }

    /**
     * Sets the {@link Stage} this <code>Character</code> is a part of.
     *
     * @param stage     the desired <code>Stage</code>.
     */
    private void setStage(Stage stage) {
        this.stage = stage;
    }

}
