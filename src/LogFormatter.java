/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 * Formats log entries. Adapted from {@link SimpleFormatter}.
 */
class LogFormatter extends SimpleFormatter {

    public synchronized String format(LogRecord record) {
        String simple = super.format(record);
        return simple.replace("sheepandwolves.", "");
    }

}
