/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Represents a game stage, encompassing the {@link Grid} and the {@link Character} instances.
 */
class Stage {

    private static final Logger l = Logger.getLogger("game");

    /**
     * The number of frames per turn. The moves of each {@link Character} are evenly distributed across the length of a
     * turn. Note that race conditions in {@link Behaviour} may emerge from very low frames per turn.
     */
    static final int NUM_FRAMES_PER_TURN = 50;

    /**
     * The path of the default stage file. If a <code>Stage</code> is created with no stage file path specified, this
     * will be loaded. If a <code>Stage</code> is created with a stage file path specified but it cannot be loaded,
     * this will be loaded instead.
     */
    private static final String DEFAULT_STAGE_FILE_PATH = "stages/stage_default.txt";

    private GameState gameState;
    private Grid grid;
    private Set<Character> chSet;
    private Direction userMove;
    private int frame;

    /**
     * Instantiates a <code>Stage</code> from the specified stage file. If the <code>Stage</code> cannot be loaded, the
     * default <code>Stage</code> is loaded instead.
     *
     * @param stageFilePath     the path of the stage file to load from.
     */
    Stage(String stageFilePath) {
        setGameState(GameState.LOADING);
        try {
            l.info("Loading Stage from \"" + stageFilePath + "\"...");
            load(stageFilePath);
            l.info("Loaded Stage successfully.");
        }
        catch (IOException e) {
            l.warning("Failed to read file \"" + stageFilePath + "\".");
            loadDefault();
        }
        catch (SyntaxException e) {
            l.warning("Failed to load Stage from \"" + stageFilePath +"\".");
            loadDefault();
        }
        setGameState(GameState.USER_TURN);
    }

    /**
     * Instantiates the default <code>Stage</code> specified by {@link #DEFAULT_STAGE_FILE_PATH}.
     */
    Stage() {
        loadDefault();
    }

    /**
     * Utility method to enable the loading of the default <code>Stage</code> from both {@link #Stage()} and
     * {@link #Stage(String)}. The latter constructor will only call this if an exception is thrown.
     */
    private void loadDefault() {
        setGameState(GameState.LOADING);
        try {
            l.info("Loading default Stage...");
            load(DEFAULT_STAGE_FILE_PATH);
        }
        catch (IOException e) {
            l.severe("Failed to read the default stage file.");
        }
        catch (SyntaxException e) {
            l.severe(e.getMessage());
            l.severe("Failed to load default Stage.");
        }
        finally {
            l.info("Loaded default Stage successfully.");
        }
        setGameState(GameState.USER_TURN);
    }

    /**
     * Loads the <code>Stage</code> from the specified stage file.
     *
     * @param stageFilePath     the path of the stage file to load from.
     * @throws IOException      if an error occurs in reading the file.
     * @throws SyntaxException  if the stage file contains a syntax error.
     * @see StageReader
     */
    private void load(String stageFilePath) throws IOException, SyntaxException {
        StageReader sr = new StageReader(stageFilePath);
        chSet = new HashSet<>();
        String command = sr.nextCommand();
        while (command != null) {
            switch (command) {
                case "grid": {
                    int dimX = sr.getDataAsInt("dim_x");
                    int dimY = sr.getDataAsInt("dim_y");
                    grid = new Grid(dimX, dimY);
                    break;
                }
                case "create_character": {
                    Cell cell;
                    if (sr.getDataAsBoolean("random_cell")) {
                        cell = grid.getRandomCell();
                    }
                    else {
                        int x = sr.getDataAsInt("x");
                        int y = sr.getDataAsInt("y");
                        cell = grid.getCellByXY(x, y);
                    }
                    switch (sr.getData("type")) {
                        case "player":      createCh(new Player(this, cell));   break;
                        case "shepherd":    createCh(new Shepherd(this, cell)); break;
                        case "sheep":       createCh(new Sheep(this, cell));    break;
                        case "wolf":        createCh(new Wolf(this, cell));     break;
                    }
                    break;
                }
                case "set_terrain": {
                    if (sr.getDataAsBoolean("use_terrain_gen")) {
                        grid.genTerrain();
                        break;
                    }
                    int x1 = sr.getDataAsInt("x1");
                    int y1 = sr.getDataAsInt("y1");
                    int x2 = sr.getDataAsInt("x2");
                    int y2 = sr.getDataAsInt("y2");
                    grid.setTerrain(x1, y1, x2, y2, Terrain.valueOf(sr.getData("type")));
                    break;
                }
            }
            command = sr.nextCommand();
        }
    }

    /**
     * Adds the specified {@link Character} to the <code>Stage</code>.
     *
     * @param ch    the <code>Character</code> to add.
     */
    private void createCh(Character ch) {
        l.info("Adding " + ch.toString() + " to Stage.");
        chSet.add(ch);
    }

    /**
     * Ends the game, recognising the outcome as either a win or a loss.
     *
     * @param gameState     A <code>GameState</code> identifying the game as a win or a loss.
     */
    void endGame(GameState gameState) {
        if (gameState == GameState.GAME_OVER_WIN || gameState == GameState.GAME_OVER_LOSS) {
            setGameState(gameState);
        }
    }

    /**
     * <p>Invoked by {@link Main}. If the key typed is <code>'w'</code>, <code>'a'</code>, <code>'s'</code>,
     * <code>'d'</code>, or the space-bar, the user move is set to <code>UP</code>, <code>LEFT</code>,
     * <code>RIGHT</code>, <code>DOWN</code>, or <code>NONE</code> respectively, and the turn transition commences.</p>
     *
     * <p>If the user attempts to move a {@link Character} outside the {@link Grid}, the turn transition commences
     * with the respective <code>Character</code> not moving at all (i.e. as if the space-bar were typed).</p>
     *
     * @param key   the key that was typed.
     */
    void inputKey(char key) {
        if (gameState != GameState.USER_TURN) {
            return;
        }
        switch (key) {
            case 'w': userMove = Direction.UP;    break;
            case 's': userMove = Direction.DOWN;  break;
            case 'a': userMove = Direction.LEFT;  break;
            case 'd': userMove = Direction.RIGHT; break;
            case ' ': userMove = Direction.NONE;  break;
            default: return;
        }
        l.info("User move set to " + userMove.toString() + ".");
        setGameState(GameState.TURN_TRANSITION);
    }

    /**
     * Invoked by {@link Main}. The mouse position is delegated to {@link Grid}.
     *
     * @param posX  the x-position of the mouse in the <code>Canvas</code>.
     * @param posY  the y-position of the mouse in the <code>Canvas</code>.
     */
    void inputMouse(int posX, int posY) {
        if (gameState == GameState.LOADING) {
            return;
        }
        grid.setHighlight(posX, posY);
    }

    /**
     * Updates the <code>Stage</code> and progresses the frame counter for the current turn if a turn transition is
     * ongoing. In particular, calls upon each {@link Character} to process the current frame. If the turn transition
     * terminates at this frame, the game state is set to <code>USER_TURN</code>.
     */
    void update() {
        if (gameState == GameState.TURN_TRANSITION) {
            for (Character ch : chSet) {
                if (gameState == GameState.TURN_TRANSITION) {
                    ch.getHead().nextFrame(frame);
                }
            }
            frame++;
            if (frame > NUM_FRAMES_PER_TURN && gameState == GameState.TURN_TRANSITION) {
                setGameState(GameState.USER_TURN);
            }
        }
    }

    /**
     * Prepares the <code>Stage</code> to commence the turn transition. In particular, starts the frame counter and
     * calls upon each {@link Character} to create a list of times (in frames) to make a move such that the time
     * between each move is evenly distributed across the duration of the turn transition.
     */
    private void nextTurn() {
        if (gameState != GameState.TURN_TRANSITION) {
            l.warning("Method called while game state is " + gameState.toString() + ".");
            return;
        }
        l.info("Commencing turn transition...");
        frame = 0;
        for (Character ch : chSet) {
            ch.getHead().nextTurn(0);
        }
        l.info("Turn transition commenced.");
    }

    /**
     * Paints the <code>Stage</code>, delegating to {@link Grid} and {@link Character}.
     *
     * @param g     the <code>Graphics</code> context to paint to.
     */
    void paint(Graphics g) {
        if (gameState == GameState.LOADING) {
            g.setColor(Main.COL_BG);
            g.fillRect(0, 0, Main.WIN_DIM_X, Main.WIN_DIM_Y);
        }
        grid.paint(g);
        for (Character ch : chSet) {
            ch.getHead().paint(g);
        }
    }

    /**
     * Returns the {@link Direction} corresponding to the move most recently set through user input.
     *
     * @return  the move most recently set through user input.
     */
    Direction getUserMove() {
        return userMove;
    }

    /**
     * Returns a {@link Character} in the <code>Stage</code> of the specified type (i.e. {@link Class}), or
     * <code>null</code> if such a <code>Character</code> does not exist.
     *
     * @param chType    the <code>Class</code> of the desired <code>Character</code>.
     * @return          a <code>Character</code> of the specified type.
     */
    Character findChType(Class chType) {
        l.info("Finding Character of type " + chType.getSimpleName() + "...");
        for (Character ch : chSet) {
            if (ch.getChType().equals(chType)) {
                l.info("Found " + ch.toString() + ".");
                return ch;
            }
        }
        l.warning("No Character of type " + chType.getSimpleName() + " exists.");
        return null;
    }

    /**
     * Sets the game state.
     *
     * @param gameState     the desired <code>GameState</code>.
     */
    private void setGameState(GameState gameState) {
        this.gameState = gameState;
        l.info("Game state changed to " + gameState.toString());
        switch (this.gameState) {
            case TURN_TRANSITION: {
                nextTurn();
                break;
            }
            case GAME_OVER_WIN: {
                l.info("Game over. The Shepherd wins.");
                break;
            }
            case GAME_OVER_LOSS: {
                l.info("Game over. The Wolf wins.");
                break;
            }
        }
    }

}
