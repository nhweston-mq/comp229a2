/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.util.logging.Logger;

/**
 * Represents a <code>Behaviour</code> whereby the {@link Character} moves towards another <code>Character</code>.
 */
class BhvrMoveTowards extends Behaviour {

    private static final Logger l = Logger.getLogger("ai");

    private Character target;

    /**
     * Creates a new <code>BhvrMoveTowards</code> for the specified {@link Character} to move towards a
     * <code>Character</code> of the specified type (i.e {@link Class}).
     *
     * @param ch            the <code>Character</code> to which this <code>Behaviour</code> is to be associated.
     * @param targetType    the <code>Class</code> of the desired target to move towards.
     */
    BhvrMoveTowards(Character ch, Class targetType) {
        super(ch);
        l.info("Instantiating Move Towards Behaviour for " + ch.toString() + " with target " + targetType.getSimpleName() + ".");
        setTarget(ch.getStage().findChType(targetType));
        l.info("Found target " + target.toString() + ".");
    }


    /**
     * <p>Returns a {@link Direction} corresponding to the move the associated {@link Character} should make.</p>
     *
     * <p>For <code>BhvrMoveTowards</code>, the <code>Direction</code> returned will be the <code>Direction</code>
     * towards the target along the orthogonal axis with the longest distance between the two <code>Character</code>
     * locations. If the distances are equal, the <code>Direction</code> along the x-direction will be returned. If the
     * two <code>Character</code> instances are already in the same <code>Cell</code>, <code>NONE</code> will be
     * returned.</p>
     *
     * @return  the <code>Direction</code> the <code>Character</code> should move in.
     */
    @Override
    Direction getNextMove() {
        if (target == null) {
            l.warning(getCh().toString() + " has no target.");
            return Direction.NONE;
        }
        int dX = target.getCell().getX() - getCh().getCell().getX();
        int dY = target.getCell().getY() - getCh().getCell().getY();
        int dXAbs = Math.abs(dX);
        int dYAbs = Math.abs(dY);
        Direction result;
        if (dX == 0 && dY == 0) {
            // Character and target occupy same Cell.
            result = Direction.NONE;
        }
        else if (dXAbs >= dYAbs) {
            // Move along the x-dimension.
            if (dX < 0) result = Direction.LEFT;
            else result = Direction.RIGHT;
        }
        else {
            // Move along the y-dimension.
            if (dY < 0) result = Direction.UP;
            else result = Direction.DOWN;
        }
        l.info(lGetNextMove(result));
        return result;
    }

    /**
     * Sets the target {@link Character} to move towards.
     *
     * @param target    the <code>Character</code> to move towards.
     */
    private void setTarget(Character target) {
        this.target = target;
    }

    /**
     * Logger utility method to obtain a description of the logic of the <code>Behaviour</code> upon making a move.
     *
     * @param dir   the <code>Direction</code> the <code>Behaviour</code> is due to return.
     * @return      a description of the logic of the <code>Behaviour</code>.
     */
    private String lGetNextMove(Direction dir) {
        return (
                getCh().toString() + " in " + getCh().getCell().toString() + " is moving " + dir.toString()
                + " to reach " + target.toString() + " in " + target.getCell().toString() + "."
        );
    }

}