/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

/**
 * Represents a <code>Wolf</code>. A <code>Wolf</code> has four movement. The game is lost if a <code>Wolf</code> is in
 * the same {@link Cell} as a {@link Sheep}.
 */
class Wolf extends Character {

    /**
     * {@inheritDoc}
     */
    Wolf(Stage stage, Cell cell) {
        super(stage, cell);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default <code>Behaviour</code> for a <code>Wolf</code> is {@link BhvrMoveTowards} with the target an
     * instance of {@link Sheep}.</p>
     */
    @Override
    void setBehaviour() {
        setBehaviour(new BhvrMoveTowards(this, Sheep.class));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void nextTurn(int numMoves) {
        super.nextTurn(numMoves + 4);
    }

}
