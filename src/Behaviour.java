/*
 * COMP229 A2
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

/**
 * Defines various behaviours to be used by {@link Character} instances at each move.
 */
abstract class Behaviour {

    private Character ch;

    /**
     * Creates a new <code>Behaviour</code> for the specified {@link Character}.
     *
     * @param ch    the <code>Character</code> to which this <code>Behaviour</code> is to be associated.
     */
    Behaviour(Character ch) {
        setCh(ch);
    }

    /**
     * Returns a {@link Direction} corresponding to the move the associated {@link Character} should make.
     *
     * @return  the <code>Direction</code> the <code>Character</code> should move in.
     */
    abstract Direction getNextMove();

    /**
     * Returns the {@link Character} associated with this <code>Behaviour</code>.
     *
     * @return  the <code>Character</code> associated with this <code>Behaviour</code>.
     */
    Character getCh() {
        return ch;
    }

    /**
     * Sets the {@link Character} associated with this <code>Behaviour</code>. Invoked only by
     * {@link #Behaviour(Character)}.
     *
     * @param ch    the <code>Character</code> to associate this <code>Behaviour</code> with.
     */
    private void setCh(Character ch) {
        this.ch = ch;
    }

}
